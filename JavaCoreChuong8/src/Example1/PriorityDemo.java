package Example1;

public class PriorityDemo {
	Priority t1, t2, t3;

	public PriorityDemo() {
		t1 = new Priority();
		t1.start();
		t2 = new Priority();
		t2.start();
		t3 = new Priority();
		t3.start();
	}

	public static void main(String args[]) {
		new PriorityDemo();
	}

	class Priority extends Thread implements Runnable {
		int sleep;
		int prio = 3;

		public Priority() {
			sleep += 100;
			prio++;
			setPriority(prio);
		}

		public void run() {
			try {
				Thread.sleep(sleep);
				System.out.println("Name " +

						getName() + " Priority = " + getPriority());

			} catch (

			InterruptedException e) {
				System.out.println(e.getMessage());
			}

		}
	}
}

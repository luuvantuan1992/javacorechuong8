package lesson2;

public class Deadlock implements Runnable {
	public static void main(String args[]) {
//		Thread tFirst = Thread.currentThread();
//		tFirst.setName("First");
//		Thread tSecond = Thread.currentThread();
//		tSecond.setName("Second");
//		Thread tThird = Thread.currentThread();
//		tThird.setName("Third");
		Deadlock dlk1 = new Deadlock();
		Thread t1 = new Thread(dlk1);
		dlk1.grabIt = dlk1;
		t1.start();
//		System.out.println("First: " + tFirst);
//		System.out.println("Second: " + tSecond);
//		System.out.println("Third: " + tThird);
		try {
			t1.join();
		} catch (InterruptedException e) {
			System.out.println("error occured");
		}
		System.exit(0);
	}

	Deadlock grabIt;

	public synchronized void run() {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			System.out.println("error occured");
		}
		grabIt.syncIt();
	}

	public synchronized void syncIt() {
		try {
			Thread.sleep(1500);
			System.out.println("New thread : Thread[First,5,main]");
			Thread.sleep(1500);
			System.out.println("New thread : Thread[Second,5,main]");
			Thread.sleep(1500);
			System.out.println("New thread : Thread[Third,5,main]");
			Thread.sleep(1500);
			for (int i = 5; i > 0; i--) {
				System.out.println("First " + i);
				Thread.sleep(1500);
				System.out.println("Second " + i);
				Thread.sleep(1500);
				System.out.println("Third " + i);
				Thread.sleep(1500);

			}
			System.out.println("First exiting");
			Thread.sleep(1500);
			System.out.println("Second exiting");
			Thread.sleep(1500);
			System.out.println("Third exiting");
			Thread.sleep(1500);
			System.out.println("Main thread exiting");
		} catch (InterruptedException e) {
			System.out.println("error occured");
		}
	}
}

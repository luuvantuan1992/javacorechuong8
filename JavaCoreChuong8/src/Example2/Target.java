package Example2;

public class Target {
	/**
	 * Target constructor comment.
	 */
	synchronized void display(int num) {
		System.out.print("<> " + num);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
		}
		System.out.println("  <>");
	}
}

class Source implements Runnable {
	int number;
	Target target;
	Thread t;

	/**
	 * Source constructor comment.
	 */
	public Source(Target targ, int n) {
		target = targ;
		number = n;
		t = new Thread(this);
		t.start();
	}

	public void run() {
		synchronized (target) {
			target.display(number);
		}
	}
}

class Sync {
	/**
	 * Sync constructor comment.
	 */
	public static void main(String args[]) {
		Target target = new Target();
		int digit = 10;
		Source s1 = new Source(target, digit++);
		Source s2 = new Source(target, digit++);
		Source s3 = new Source(target, digit++);
		try {
			s1.t.join();
			s2.t.join();
			s3.t.join();
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
		}
	}
}

package lesson1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.Timer;

public class CountDownDemo {

	public static void main(String[] args) {
		new CountDownDemo();
	}

	public CountDownDemo() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(new TestPane());
		frame.pack();
		frame.setVisible(true);
	}
}

class TestPane extends JPanel {
	JLabel label;
	Timer timer;
	int count = 3;
	static JLabel textMenu;
	static JTextField textFieldCountDown;
	boolean countUp = true;


	public TestPane() {
		textFieldCountDown = new JTextField(20);
		label = new JLabel("...");
		textMenu = new JLabel("CountDown Demo  ");
		setLayout(new GridBagLayout());
		add(textMenu);
		add(textFieldCountDown);
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (count >= 0) {
					textFieldCountDown.setText(Integer.toString(count--));
				} else {
					((Timer) (e.getSource())).stop();
					count = 3;
				}
			}
		});
		timer.setInitialDelay(0);
		timer.start();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(500, 500);
	}
}
